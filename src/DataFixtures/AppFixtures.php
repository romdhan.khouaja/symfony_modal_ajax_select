<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Note;
use App\Entity\Person; 
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $notes = [];
        for($i = 0; $i < 21; $i++){
            $note = new Note();
            $note->setValeur(mt_rand(5, 20));
            $manager->persist($note); 
            $notes[] = $note;
        }

        for ($i = 0; $i < 20; $i++) {
            $person = new Person();
            $person->setFirstname($faker->firstName);
            $person->setLastname($faker->lastName);
            for($j=0;$j<10;$j++){
                $person->addNote($faker->randomElement($notes) );
            } 
            $manager->persist($person);

        }

        $manager->flush();
    }
}
